from flask import Flask, render_template, redirect 
import links_from_header
import requests
import json

repertoire = Flask(__name__, template_folder='./templates/')

# fonction pour construire la home page
@repertoire.route('/') 
def home():
  r = requests.get(
      'http://repertoire.ecrituresnumeriques.ca/api/items')
  data=json.loads(r.text)
  headers=r.headers #request allows to extract the headers
  header=headers['Link'] # in the headers json there is a field Link
  linkheader=links_from_header.extract(header) # using links_from_headers to tranform Link value in a json with the keys prev next last
  lastlink=linkheader['last'] # picking the value of the key last 
  lastpage=int(lastlink.replace('http://repertoire.ecrituresnumeriques.ca/api/items?sort_by=id&sort_order=asc&page=', '')) # I need the number of the last page which is at the end of the string, so I delete the beginning of the string
  # print(str(lastpage))
  maxpage=lastpage+1 #to be sure to parse all the pages
  count=1
  while count < maxpage:
      print('ok')
      count=count+1
      r1 = requests.get('http://repertoire.ecrituresnumeriques.ca/api/items?sort_by=id&sort_order=asc&page='+str(count))
      data2=json.loads(r1.text)
      for i in data2:
          data.append(i)
  return render_template('home.html',data=data, headers=r1.headers)

if __name__ == '__main__':
  repertoire.run(host='0.0.0.0', debug=True)

  # exemple: http://repertoire.ecrituresnumeriques.ca/api/items?sort_by=dcterms:date
  
  #ce que je voudrais faire :
  #- recuperer les entrees du 1er parcours (espace)
  #- afficher image et titre en dessous, cote à cote : defilement latéral
  #- que chaque entree renvoie a la fiche de l'œuvre (dans un second temps je veux faire des pages pour chacune mais pour l'instant faisons comme ça)
  #- afficher le titre du parcours en vertical le long de la bande ainsi formée
  
  # endessous :
    #- recuperer les entrees du 2eme parcours (fantastique)
  #- afficher image et titre en dessous, cote a cote : defilement lateral
  #- que chaque entree renvoie a la fiche de l'œuvre
  #- afficher le titre du parcours en vertical le long de la bande ainsi formee
  
  # endessous :
    #- recuperer les entrees du 3eme parcours (feminitude)
  #- afficher image et titre en dessous, cote a cote : defilement latéral
  #- que chaque entree renvoie à la fiche de l'œuvre
  #- afficher le titre du parcours en vertical le long de la bande ainsi formee

#DEUXIEME temps
#- pour chaque parcours quand on clique sur une œuvre on arriverait sur une page affichant la fiche dans une interface permettant via un defilement vertical la navigation entre les fiches du parcours
#- fiche affichant les infos de l'œuvre (colone de gauche : image(s) descriprion en dessous colonne de droite : titre, auteurice, cms, support, technique, genre, thematique. logos pour mecanique, et format.
#- possibilite davoir plusuieurs captations visuelles
#- styler en pile de cartes
#- conserver en bandeau le titre du parcours (clic retour vers home) et des sous parcours (clic affichage des oeuvres du sous parcours)

#TROISIEME temps
#- limiter le nb d'entrees affichees par parcours par selection aleatoire, les encapsuler dans des boites cliqables (meme effets que précédemment) pour mettre dans le defilment latéral des sous-parcours
#- modifier a chaque affichage l'ordre des parcours affiches
#- ajouter bandeau en haut avec le logo de la chaire et un i pour definition du projet

