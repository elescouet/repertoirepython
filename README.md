# README

Cloner le repo.

Créer l'environnement virtuel et faire tourner l'application:

```
python3 -m venv venv
source venv/bin/activate
pip install Flask
pip install links-from-link-header
python repertoire.py
```

Maintenant vous pouvez voir l'application sur `http://localhost:5000/`
